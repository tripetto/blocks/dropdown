/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class DropdownCondition extends ConditionBlock<{
    readonly option: string | undefined;
}> {
    @condition
    isSelected(): boolean {
        const dropdownSlot = this.valueOf<string>();

        return (
            (dropdownSlot &&
                (dropdownSlot.reference === this.props.option ||
                    (!this.props.option && !dropdownSlot.reference))) ||
            false
        );
    }
}
