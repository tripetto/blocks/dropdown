/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    isBoolean,
    metadata,
    npgettext,
    pgettext,
    slots,
    supplies,
    tripetto,
} from "@tripetto/builder";
import { DropdownOption } from "./option";
import { DropdownCondition } from "./conditions/dropdown";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_NOTHING from "../../assets/nothing.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:dropdown", "Dropdown (single-select)");
    },
})
export class Dropdown extends NodeBlock {
    dropdownSlot!: Slots.String;

    @definition("items")
    @affects("#label")
    @supplies<Dropdown>("#slot", "option")
    readonly options = Collection.of<DropdownOption, Dropdown>(
        DropdownOption,
        this
    );

    @definition("boolean", "optional")
    randomize?: boolean;

    get label() {
        return npgettext(
            "block:dropdown",
            "%2 (%1 option)",
            "%2 (%1 options)",
            this.options.count,
            this.type.label
        );
    }

    @metadata("calculator")
    get calculator() {
        return {
            option: {
                allowCastToNumber: true,
            },
        };
    }

    @slots
    defineSlots(): void {
        this.dropdownSlot = this.slots.static({
            type: Slots.String,
            reference: "option",
            label: pgettext("block:dropdown", "Selected option"),
            exchange: ["required", "alias", "exportable"],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.groups.general();
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        const collection = this.editor.collection({
            collection: this.options,
            title: pgettext("block:dropdown", "Dropdown options"),
            placeholder: pgettext("block:dropdown", "Unnamed option"),
            autoOpen: true,
            allowVariables: true,
            allowImport: true,
            allowExport: true,
            allowDedupe: true,
            showAliases: true,
            sorting: "manual",
            emptyMessage: pgettext(
                "block:dropdown",
                "Click the + button to add an option..."
            ),
        });

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:dropdown", "Randomization"),
            form: {
                title: pgettext("block:dropdown", "Randomization"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:dropdown",
                            "Randomize the options (using [Fisher-Yates shuffle](%1))",
                            "https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle"
                        ),
                        Forms.Checkbox.bind(this, "randomize", undefined, true)
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.randomize),
        });

        this.editor.groups.options();
        this.editor.required(this.dropdownSlot);
        this.editor.visibility();

        this.editor.scores({
            target: this,
            collection,
            description: pgettext(
                "block:dropdown",
                "Generates a score based on the selected option. Open the settings panel for each option to set the score."
            ),
        });

        this.editor.alias(this.dropdownSlot);
        this.editor.exportable(this.dropdownSlot);
    }

    @conditions
    defineConditions(): void {
        this.options.each((option: DropdownOption) => {
            if (option.name) {
                this.conditions.template({
                    condition: DropdownCondition,
                    label: option.name,
                    burst: "branch",
                    props: {
                        slot: this.dropdownSlot,
                        option: option,
                    },
                });
            }
        });

        this.conditions.template({
            condition: DropdownCondition,
            label: pgettext("block:dropdown", "Nothing selected"),
            icon: ICON_NOTHING,
            separator: true,
            props: {
                slot: this.dropdownSlot,
                option: undefined,
            },
        });

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(score.label, ICON_SCORE);

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext("block:dropdown", "Score is equal to"),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:dropdown",
                            "Score is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:dropdown",
                            "Score is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:dropdown",
                            "Score is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext("block:dropdown", "Score is between"),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:dropdown",
                            "Score is not between"
                        ),
                    },
                    {
                        mode: "defined",
                        label: pgettext(
                            "block:dropdown",
                            "Score is calculated"
                        ),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:dropdown",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
